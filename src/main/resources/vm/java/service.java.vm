package ${packageName}.service;

import ${packageName}.entity.${ClassName};
import com.mars.common.response.PageInfo;
import ${packageName}.request.${ClassName}Request;
import java.util.List;

/**
 * ${functionName}接口
 *
 * @author ${author}
 * @date ${datetime}
 */
public interface I${ClassName}Service {
    /**
     * 新增
     *
     * @param param param
     * @return ${ClassName}
     */
    ${ClassName} add(${ClassName}Request param);

    /**
     * 删除
     *
     * @param id id
     * @return boolean
     */
    boolean delete(${pkColumn.javaType} ${pkColumn.javaField});

    /**
     * 批量删除
     *
     * @param ids ids
     * @return boolean
     */
    boolean deleteBatch(List<${pkColumn.javaType}> ${pkColumn.javaField}s);

    /**
     * 更新
     *
     * @param param param
     * @return boolean
     */
    boolean update(${ClassName}Request param);

    /**
     * 查询单条数据，Specification模式
     *
     * @param id id
     * @return ${ClassName}
     */
    ${ClassName} getById(${pkColumn.javaType} ${pkColumn.javaField});

    /**
     * 查询分页数据
     *
     * @param param param
     * @return PageInfo<${ClassName}>
     */
    PageInfo<${ClassName}> pageList(${ClassName}Request param);

}
