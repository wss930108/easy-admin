package ${packageName}.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mars.common.response.PageInfo;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import ${packageName}.request.${ClassName}Request;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import ${packageName}.mapper.${ClassName}Mapper;
import org.springframework.beans.BeanUtils;
import ${packageName}.entity.${ClassName};
import com.baomidou.mybatisplus.core.metadata.IPage;
import ${packageName}.service.I${ClassName}Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * ${functionName}业务层处理
 *
 * @author ${author}
 * @date ${datetime}
 */
@Slf4j
@Service
@AllArgsConstructor
public class ${ClassName}ServiceImpl implements I${ClassName}Service {

    private final ${ClassName}Mapper baseMapper;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ${ClassName} add(${ClassName}Request request) {
        ${ClassName} entity = new ${ClassName}();
        BeanUtils.copyProperties(request, entity);
        baseMapper.insert(entity);
        return entity;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean delete(${pkColumn.javaType} ${pkColumn.javaField}) {
        return baseMapper.deleteById(${pkColumn.javaField}) > 0;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean deleteBatch(List<${pkColumn.javaType}> ${pkColumn.javaField}s) {
        return baseMapper.deleteBatchIds(${pkColumn.javaField}s) > 0;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean update(${ClassName}Request request) {
        ${ClassName} entity = new ${ClassName}();
        BeanUtils.copyProperties(request, entity);
        baseMapper.updateById(entity);
        return baseMapper.updateById(entity) > 0 ;
    }

    @Override
    public ${ClassName} getById(${pkColumn.javaType} ${pkColumn.javaField}) {
        return baseMapper.selectById(${pkColumn.javaField});
    }

    @Override
    public PageInfo<${ClassName}> pageList(${ClassName}Request request) {
        Page<${ClassName}> page = new Page<>(request.getPageNo(), request.getPageSize());
        LambdaQueryWrapper<${ClassName}> query = this.buildWrapper(request);
        IPage<${ClassName}> pageRecord = baseMapper.selectPage(page, query);
        return PageInfo.build(pageRecord);
    }

    private LambdaQueryWrapper<${ClassName}> buildWrapper(${ClassName}Request param) {
        LambdaQueryWrapper<${ClassName}> query = new LambdaQueryWrapper<>();
#foreach($column in $columns)
#set($queryType=$column.queryType)
#set($javaField=$column.javaField)
#set($javaType=$column.javaType)
#set($columnName=$column.columnName)
#set($AttrName=$column.javaField.substring(0,1).toUpperCase() + ${column.javaField.substring(1)})
#if($column.query)
#if($column.queryType == "EQ")
#if($javaType == 'String')
        if (StringUtils.isNotBlank(param.get$AttrName())){
               query.eq(${ClassName}::get$AttrName ,param.get$AttrName());
        }
#else
        if (param.get$AttrName() != null){
               query.eq(${ClassName}::get$AttrName ,param.get$AttrName());
        }
#end
#elseif($queryType == "NE")
#if($javaType == 'String')
        if (StringUtils.isNotBlank(param.get$AttrName())){
                query.ne(${ClassName}::get$AttrName ,param.get$AttrName());
        }
#else
        if (param.get$AttrName() != null){
                query.ne(${ClassName}::get$AttrName ,param.get$AttrName());
        }
#end
#elseif($queryType == "GT")
#if($javaType == 'String')
        if (StringUtils.isNotBlank(param.get$AttrName())){
                query.gt(${ClassName}::get$AttrName ,param.get$AttrName());
        }
#else
        if (param.get$AttrName() != null){
                query.gt(${ClassName}::get$AttrName ,param.get$AttrName());
        }
#end
#elseif($queryType == "GTE")
#if($javaType == 'String')
        if (StringUtils.isNotBlank(param.get$AttrName())){
                query.ge(${ClassName}::get$AttrName ,param.get$AttrName());
        }
#else
        if (param.get$AttrName() != null){
                query.ge(${ClassName}::get$AttrName ,param.get$AttrName());
        }
#end
#elseif($queryType == "LT")
#if($javaType == 'String')
       if (StringUtils.isNotBlank(param.get$AttrName())){
               query.lt(${ClassName}::get$AttrName ,param.get$AttrName());
       }
#else
       if (param.get$AttrName() != null){
               query.lt(${ClassName}::get$AttrName ,param.get$AttrName());
       }
#end
#elseif($queryType == "LTE")
#if($javaType == 'String')
        if (StringUtils.isNotBlank(param.get$AttrName())){
               query.le(${ClassName}::get$AttrName ,param.get$AttrName());
        }
#else
        if (param.get$AttrName() != null){
               query.le(${ClassName}::get$AttrName ,param.get$AttrName());
        }
#end
#elseif($queryType == "LIKE")
 #if($javaType == 'String')
        if (StringUtils.isNotBlank(param.get$AttrName())){
               query.like(${ClassName}::get$AttrName ,param.get$AttrName());
        }
#else
        if (param.get$AttrName() != null){
               query.like(${ClassName}::get$AttrName ,param.get$AttrName());
        }
#end
#elseif($queryType == "BETWEEN")
#end
#end
#end
        return query;
    }

}
