function getQueryString(name) {
    let reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
    let r = window.location.search.substr(1).match(reg);
    if (r != null) return decodeURI(r[2]);
    return null;
}

/**
 * 打开页面
 * @param title
 * @param url
 * @param vueObj
 */
function openPage(title, url, vueObj) {
    parent.layer.open({
        type: 2,
        title: title,
        area: ['800px', '550px'],
        content: url,
        end: function () {
            vueObj.handleQuery();
        }
    });
}

/**
 * 根据字典类型查询字典数据信息
 * @param dictType
 * @returns {*}
 */
function getDicts(dictType) {
    return requests({
        url: '/admin/sysDictData/getDictData/' + dictType,
        method: 'get'
    })
}

/**
 * 回显数据字典
 * @param datas
 * @param value
 * @returns {string}
 */
function selectDictLabel(datas, value) {
    var actions = [];
    Object.keys(datas).map((key) => {
        if (datas[key].dictValue == ('' + value)) {
            actions.push(datas[key].dictLabel);
            return false;
        }
    })
    return actions.join('');
}

/**
 * 关闭页面
 */
function closePage() {
    let index = parent.layer.getFrameIndex(window.name);
    parent.layer.close(index);
}

/**
 * 查询
 * @param url
 * @param queryParam
 * @param vueObj
 */
function query(url, queryParam, vueObj) {
    requests.post(url, queryParam)
        .then((res) => {
            if (res.code === '200') {
                if (res.data.count) {
                    vueObj.tableData = res.data.list;
                    vueObj.total = res.data.count;
                } else {
                    vueObj.tableData = res.data;
                }
            }
        });
}

/**
 * 删除
 * @param url
 * @param vueObj
 */
function deleteById(url, vueObj) {
    parent.layer.confirm('<font color="red">确定删除该数据吗?</font>', {
        icon: 3,
        title: "删除数据操作",
        btn: ['确定', '取消']

    }, function (index) {
        requests.post(url)
            .then((res) => {
                if (res.code === '200') {
                    parent.layer.msg('删除成功');
                    parent.layer.close(index);
                    vueObj.handleQuery();
                } else {
                    parent.layer.msg(res.message);
                }
            });
    });
}

/**
 * 新增或修改
 * @param url
 * @param vueObj
 */
function addOrUpdate(url, vueObj) {
    vueObj.$refs.formData.validate((valid) => {
        if (valid) {
            let param = vueObj.formData;
            requests.post(url, JSON.parse(JSON.stringify(param)))
                .then((res) => {
                    if (res.code === '200') {
                        if (url.indexOf('/add') > 0) {
                            parent.layer.msg('新增成功');
                        } else if (url.indexOf('/update') > 0) {
                            parent.layer.msg('修改成功');
                        }
                        closePage();
                    } else {
                        parent.layer.msg(res.message);
                    }
                });
        } else {
            return false;
        }
    });
}

/**
 * 导出
 * @param url
 * @param queryParam
 */
function exports(url, queryParam) {
    requests.post(url, queryParam, {responseType: "arraybuffer"})
        .then((res) => {
            let disposition = res.headers['content-disposition'];
            let fileName = decodeURI(disposition.substring(disposition.indexOf('filename=') + 9, disposition.length));
            let blob = new Blob([res.data]);
            let link = document.createElement('a');
            link.href = window.URL.createObjectURL(blob);
            link.download = fileName;
            link.click();
            link.remove();
        });
}


/**
 * 打开页面
 * @param title
 * @param url
 * @param vueObj
 */
function openMiddlePage(title, url, vueObj) {
    parent.layer.open({
        type: 2,
        title: title,
        area: ['1400px', '900px'],
        content: url,
        end: function () {
            vueObj.handleQuery();
        }
    });
}


