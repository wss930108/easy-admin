package com.mars.router;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;


/**
 * 路由控制器
 *
 * @author 源码字节-程序员Mars
 */
@Controller
public class RouterController {

    @GetMapping("/login")
    public String login() {
        return "login";
    }

    @GetMapping("/")
    public String index() {
        return "index";
    }

    @GetMapping("/home/home.html")
    public String home() {
        return "home/home";
    }

    @GetMapping("/info/info.html")
    public String info() {
        return "info/info";
    }

    @GetMapping("/log/api.html")
    public String log() {
        return "log/api";
    }

    @GetMapping("/menu/menu.html")
    public String menu() {
        return "menu/menu";
    }

    @GetMapping("/menu/menuAdd.html")
    public String menuAdd() {
        return "menu/menuAdd";
    }

    @GetMapping("/menu/menuUpdate.html")
    public String menuUpdate() {
        return "menu/menuUpdate";
    }

    @GetMapping("/role/role.html")
    public String role() {
        return "role/role";
    }

    @GetMapping("/role/roleAdd.html")
    public String roleAdd() {
        return "role/roleAdd";
    }

    @GetMapping("/role/roleUpdate.html")
    public String roleUpdate() {
        return "role/roleUpdate";
    }

    @GetMapping("/tool/api.html")
    public String toolApi() {
        return "tool/api";
    }

    @GetMapping("/gen/import-table.html")
    public String genImportTable() {
        return "gen/import-table";
    }

    @GetMapping("/gen/import-table-update.html")
    public String genImportTableUpdate() {
        return "gen/import-table-update";
    }

    @GetMapping("/gen/table.html")
    public String tableList() {
        return "gen/table";
    }

    @GetMapping("/user/user.html")
    public String user() {
        return "user/user";
    }

    @GetMapping("/user/userAdd.html")
    public String userAdd() {
        return "user/userAdd";
    }

    @GetMapping("/user/userUpdate.html")
    public String userUpdate() {
        return "user/userUpdate";
    }

    @GetMapping("/user/userImport.html")
    public String userImport() {
        return "user/userImport";
    }

    @GetMapping("/post/post.html")
    public String post() {
        return "post/post";
    }

    @GetMapping("/post/post-add-update.html")
    public String postAddUpdate() {
        return "post/post-add-update";
    }

    @GetMapping("/resourceInfo/resourceInfo.html")
    public String resourceInfo() {
        return "resourceInfo/resourceInfo";
    }

    @GetMapping("/resourceInfo/resourceInfo-add-update.html")
    public String resourceInfoAddUpdate() {
        return "resourceInfo/resourceInfo-add-update";
    }

    @GetMapping("/classInfo/classInfo.html")
    public String classInfo() {
        return "classInfo/classInfo";
    }

    @GetMapping("/classInfo/classInfo-add-update.html")
    public String classInfoAddUpdate() {
        return "classInfo/classInfo-add-update";
    }

    @GetMapping("/starCource/starCource.html")
    public String starCource() {
        return "starCource/starCource";
    }

    @GetMapping("/starCource/starCource-add-update.html")
    public String starCourceAddUpdate() {
        return "starCource/starCource-add-update";
    }

    @GetMapping("/gen/create-table.html")
    public String createTable() {
        return "gen/create-table";
    }

    @GetMapping("/gen/code-remove.html")
    public String codeRemove() {
        return "gen/code-remove";
    }

    @GetMapping("/sysLoginRecord/sysLoginRecord.html")
    public String sysLoginRecord() {
        return "sysLoginRecord/sysLoginRecord";
    }

    @GetMapping("/sysLoginRecord/sysLoginRecord-add-update.html")
    public String sysLoginRecordAddUpdate() {
        return "sysLoginRecord/sysLoginRecord-add-update";
    }

    @GetMapping("/sysOperLog/sysOperLog.html")
    public String sysOperLog() {
        return "sysOperLog/sysOperLog";
    }

    @GetMapping("/sysOperLog/sysOperLog-add-update.html")
    public String sysOperLogAddUpdate() {
        return "sysOperLog/sysOperLog-add-update";
    }

    @GetMapping("/sysDictType/sysDictType.html")
    public String sysDictType() {
        return "sysDictType/sysDictType";
    }

    @GetMapping("/sysDictType/sysDictType-add-update.html")
    public String sysDictTypeAddUpdate() {
        return "sysDictType/sysDictType-add-update";
    }

    @GetMapping("/sysDictData/sysDictData.html")
    public String sysDictData() {
        return "sysDictData/sysDictData";
    }

    @GetMapping("/sysDictData/sysDictData-add-update.html")
    public String sysDictDataAddUpdate() {
        return "sysDictData/sysDictData-add-update";
    }

    @GetMapping("/apTest2/apTest2-add-update.html")
    public String apTest2AddUpdate() {
        return "apTest2/apTest2-add-update";
    }

    @GetMapping("/sysConfig/sysConfig.html")
    public String sysConfig() {
        return "sysConfig/sysConfig";
    }

    @GetMapping("/sysConfig/sysConfig-add-update.html")
    public String sysConfigAddUpdate() {
        return "sysConfig/sysConfig-add-update";
    }

    @GetMapping("/sysOss/sysOss.html")
    public String sysOss() {
        return "sysOss/sysOss";
    }

    @GetMapping("/sysOss/sysOss-add-update.html")
    public String sysOssAddUpdate() {
        return "sysOss/sysOss-add-update";
    }


}
