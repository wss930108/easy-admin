package com.mars.framework.router;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.io.*;


/**
 * 功能描述
 *
 * @author Mars.wq [wqexpore@163.com]
 * @version 1.0
 * @date 2023-11-07 17:27:35
 */
@Slf4j
@Component
public class DynamicRoute {


    public static void main(String[] args) throws Exception {
        remove("apTest1");
    }

    /**
     * 删除路由
     *
     * @param businessName businessName
     */
    public static void remove(String businessName) {
        try {
            removeRouter(businessName);
            removeRouter(businessName + "AddUpdate");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 动态删除路由
     *
     * @param businessName businessName
     */
    private static void removeRouter(String businessName) throws IOException {
        String dir = System.getProperty("user.dir");
        String filePath = (dir + "\\src\\main\\java\\com\\mars\\router\\RouterController.java");
        // 读取源代码文件
        StringBuilder sourceCode = new StringBuilder();
        try (BufferedReader reader = new BufferedReader(new FileReader(filePath))) {
            String line;
            while ((line = reader.readLine()) != null) {
                sourceCode.append(line).append(System.lineSeparator());
            }
        }

        // 删除目标方法及其注解
        int methodStartIndex = sourceCode.indexOf("public String " + businessName);
        if (methodStartIndex != -1) {
            int methodEndIndex = sourceCode.indexOf("}", methodStartIndex);
            if (methodEndIndex != -1) {
                // 删除方法的代码块
                sourceCode.delete(methodStartIndex, methodEndIndex + 1);

                // 删除方法上的注解及其后的空行
                int methodAnnotationStartIndex = sourceCode.lastIndexOf("@", methodStartIndex);
                if (methodAnnotationStartIndex != -1) {
                    int methodAnnotationEndIndex = sourceCode.indexOf("\n", methodAnnotationStartIndex);
                    if (methodAnnotationEndIndex != -1) {
                        int emptyLineIndex = sourceCode.indexOf("\n", methodAnnotationEndIndex + 1);
                        if (emptyLineIndex != -1) {
                            sourceCode.delete(methodAnnotationStartIndex, emptyLineIndex + 1);
                        }
                    }
                }
            }
        }

        // 重新写入源代码文件
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(filePath))) {
            writer.write(sourceCode.toString());
        }

        System.out.println("方法、注解及空行已删除");
    }


    /**
     * 根据功能名称写入路由
     *
     * @param businessName businessName
     */
    public static void writeRouter(String businessName) {
        String dir = System.getProperty("user.dir");
        File file = new File(dir + "\\src\\main\\java\\com\\mars\\router\\RouterController.java");
        try {
            // 读取原文件内容
            BufferedReader reader = new BufferedReader(new FileReader(file));
            StringBuilder fileContent = new StringBuilder();
            String line;
            String lastLine = null;
            while ((line = reader.readLine()) != null) {
                if (lastLine != null) {
                    fileContent.append(lastLine).append(System.lineSeparator());
                }
                lastLine = line;
            }
            reader.close();
            int lastBraceIndex = lastLine.lastIndexOf("}");
            if (lastBraceIndex >= 0) {
                lastLine = lastLine.substring(0, lastBraceIndex);
            }
            int secondLastLineIndex = fileContent.lastIndexOf(System.lineSeparator(), fileContent.lastIndexOf(System.lineSeparator()) - 1);

            String newContent1 = "\n    @GetMapping(\"/" + businessName + "/" + businessName + ".html\")\n";
            newContent1 += "    public String " + businessName + "() {\n";
            newContent1 += "        return \"" + businessName + "/" + businessName + "\";\n";
            newContent1 += "    }\n";

            String newContent = "\n    @GetMapping(\"/" + businessName + "/" + businessName + "-add-update.html\")\n";
            newContent += "    public String " + businessName + "AddUpdate() {\n";
            newContent += "        return \"" + businessName + "/" + businessName + "-add-update\";\n";
            newContent += "    }\n";

            fileContent.insert(secondLastLineIndex, newContent);
            fileContent.insert(secondLastLineIndex, newContent1);
            FileWriter writer = new FileWriter(file);
            writer.write(fileContent.toString());
            writer.write("}");
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
