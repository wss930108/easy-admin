package com.mars.module.tool.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * 功能描述
 *
 * @author Mars.wq [wqexpore@163.com]
 * @version 1.0
 * @date 2023-11-13 23:02:58
 */
@Data
@ApiModel(value = "建表行请求参数")
public class FieldRequest {

    /**
     * 字段名称
     */
    @ApiModelProperty(value = "字段名称")
    private String fieldName;
    /**
     * 字段类型
     */
    @ApiModelProperty(value = "字段类型")
    private String fieldType;
    /**
     * 字段长度
     */
    @ApiModelProperty(value = "字段长度")
    private Integer fieldLength;
    /**
     * 字段排序
     */
    @ApiModelProperty(value = "字段排序")
    private Integer sort;
    /**
     * 字段注释
     */
    @ApiModelProperty(value = "字段注释")
    private String fieldComment;

    /**
     * 是否允许为空
     */
    @ApiModelProperty(value = "是否为空 默认为空")
    private boolean defaultNull = true;

    /**
     * 默认值
     */
    @ApiModelProperty(value = "默认值")
    private String defaultValue;
}
