package com.mars.module.tool.controller;

import com.mars.common.enums.BusinessType;
import com.mars.common.request.tool.GenCodeDeployRequest;
import com.mars.common.request.tool.GenCodeRemoveRequest;
import com.mars.common.result.R;
import com.mars.common.request.tool.TableQueryRequest;
import com.mars.framework.annotation.Log;
import com.mars.module.tool.entity.GenTable;
import com.mars.module.tool.request.CreateTableRequest;
import com.mars.module.tool.request.GenTableImportRequest;
import com.mars.module.tool.request.GenTableRequest;
import com.mars.module.tool.service.IGenTableService;
import com.mars.module.tool.service.ITableService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLException;

/**
 * 数据库表Controller
 *
 * @author 源码字节-程序员Mars
 */
@Api(tags = "工具管理-数据库表")
@RestController
@RequestMapping("/tool/table")
@AllArgsConstructor
public class GenTableController {

    private final ITableService tableService;

    private final IGenTableService genTableService;


    /**
     * 查询已导入表列表
     *
     * @param request request
     * @return R
     */
    @PostMapping("/genTableList")
    @ApiOperation(value = "查询已导入表列表")
    public R genTableList(@RequestBody GenTableRequest request) {
        return R.success(genTableService.genTableList(request));
    }


    /**
     * 获取数据库表列表
     *
     * @param request request
     * @return R
     */
    @PostMapping("/tableList")
    @ApiOperation(value = "获取表列表")
    public R list(@RequestBody TableQueryRequest request) {
        return R.success(tableService.list(request));
    }

    /**
     * 删除已导入表
     *
     * @param id id
     * @return R
     */
    @Log(title = "删除已导入表", businessType = BusinessType.DELETE)
    @ApiOperation(value = "删除已导入表")
    @PostMapping("/delete/{id}")
    public R delete(@PathVariable("id") Long id) {
        genTableService.deleteGenTableById(id);
        return R.success();
    }


    /**
     * 已导入表 详情
     *
     * @param id id
     * @return R
     */
    @ApiOperation(value = "导入表详情")
    @GetMapping("/detail/{id}")
    public R detail(@PathVariable("id") Long id) {
        return R.success(genTableService.detail(id));
    }

    /**
     * 已导入表 更新
     *
     * @param genTable genTable
     * @return R
     */
    @Log(title = "更新已导入表", businessType = BusinessType.UPDATE)
    @ApiOperation(value = "已导入表 更新")
    @PostMapping("/update")
    public R updateGenTable(@RequestBody GenTable genTable) {
        genTableService.updateGenTable(genTable);
        return R.success();
    }

    /**
     * 生成代码+一键部署
     *
     * @param request 请求参数
     * @return 数据
     */
    @Log(title = "一键部署", businessType = BusinessType.INSERT)
    @PostMapping("/deploy")
    public R deploy(@RequestBody GenCodeDeployRequest request) {
        genTableService.deploy(request);
        return R.success();
    }

    /**
     * 卸载一键部署的内容
     *
     * @param request 请求参数
     * @return 数据
     */
    @Log(title = "一键卸载", businessType = BusinessType.DELETE)
    @ApiOperation(value = "卸载一键部署的内容")
    @PostMapping("/deployRemove")
    public R deployRemove(@RequestBody GenCodeRemoveRequest request) {
        genTableService.remove(request);
        return R.success();
    }


    /**
     * 导入表结构（保存）
     */
    @Log(title = "导入表结构", businessType = BusinessType.INSERT)
    @ApiOperation(value = "导入表结构")
    @PostMapping("/importTable")
    public R importTableSave(@RequestBody GenTableImportRequest request) {
        genTableService.importGenTable(request);
        return R.success();
    }


    /**
     * 一键建表
     */
    @Log(title = "一键建表", businessType = BusinessType.INSERT)
    @ApiOperation(value = "一键建表")
    @PostMapping("/createTable")
    public R createTable(@RequestBody CreateTableRequest request) throws SQLException {
        genTableService.createTable(request);
        return R.success();
    }


}
