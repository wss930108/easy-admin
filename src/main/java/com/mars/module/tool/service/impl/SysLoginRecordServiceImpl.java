package com.mars.module.tool.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mars.common.response.PageInfo;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import com.mars.module.tool.request.SysLoginRecordRequest;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import com.mars.module.tool.mapper.SysLoginRecordMapper;
import org.springframework.beans.BeanUtils;
import com.mars.module.tool.entity.SysLoginRecord;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mars.module.tool.service.ISysLoginRecordService;

import java.util.List;

/**
 * 系统访问记录业务层处理
 *
 * @author mars
 * @date 2023-11-17
 */
@Slf4j
@Service
@AllArgsConstructor
public class SysLoginRecordServiceImpl implements ISysLoginRecordService {

    private final SysLoginRecordMapper baseMapper;

    @Override
    public SysLoginRecord add(SysLoginRecordRequest request) {
        SysLoginRecord entity = new SysLoginRecord();
        BeanUtils.copyProperties(request, entity);
        baseMapper.insert(entity);
        return entity;
    }

    @Override
    public boolean delete(Long infoId) {
        return baseMapper.deleteById(infoId) > 0;
    }

    @Override
    public boolean deleteBatch(List<Long> infoIds) {
        return baseMapper.deleteBatchIds(infoIds) > 0;
    }

    @Override
    public boolean update(SysLoginRecordRequest request) {
        SysLoginRecord entity = new SysLoginRecord();
        BeanUtils.copyProperties(request, entity);
        baseMapper.updateById(entity);
        return baseMapper.updateById(entity) > 0;
    }

    @Override
    public SysLoginRecord getById(Long infoId) {
        return baseMapper.selectById(infoId);
    }

    @Override
    public PageInfo<SysLoginRecord> pageList(SysLoginRecordRequest request) {
        Page<SysLoginRecord> page = new Page<>(request.getPageNo(), request.getPageSize());
        LambdaQueryWrapper<SysLoginRecord> query = this.buildWrapper(request);
        IPage<SysLoginRecord> pageRecord = baseMapper.selectPage(page, query);
        return PageInfo.build(pageRecord);
    }

    private LambdaQueryWrapper<SysLoginRecord> buildWrapper(SysLoginRecordRequest param) {
        LambdaQueryWrapper<SysLoginRecord> query = new LambdaQueryWrapper<>();
        if (StringUtils.isNotBlank(param.getUserName())) {
            query.like(SysLoginRecord::getUserName, param.getUserName());
        }
        if (StringUtils.isNotBlank(param.getIpaddr())) {
            query.like(SysLoginRecord::getIpaddr, param.getIpaddr());
        }
        if (StringUtils.isNotBlank(param.getLoginLocation())) {
            query.like(SysLoginRecord::getLoginLocation, param.getLoginLocation());
        }
        if (StringUtils.isNotBlank(param.getBrowser())) {
            query.like(SysLoginRecord::getBrowser, param.getBrowser());
        }
        if (StringUtils.isNotBlank(param.getOs())) {
            query.like(SysLoginRecord::getOs, param.getOs());
        }
        if (StringUtils.isNotBlank(param.getStatus())) {
            query.like(SysLoginRecord::getStatus, param.getStatus());
        }
        if (StringUtils.isNotBlank(param.getMsg())) {
            query.like(SysLoginRecord::getMsg, param.getMsg());
        }
        if (param.getLoginTime() != null) {
            query.like(SysLoginRecord::getLoginTime, param.getLoginTime());
        }
        query.orderByDesc(SysLoginRecord::getLoginTime);
        return query;
    }

}
