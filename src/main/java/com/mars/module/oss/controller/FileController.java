package com.mars.module.oss.controller;

import com.mars.common.result.R;
import com.mars.module.oss.service.IFileService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;

/**
 * @author 程序员Mars
 */
@Api(tags = "图片上传接口")
@RestController
@RequestMapping("/common/upload")
public class FileController {

    @Resource
    private IFileService fileService;

    /**
     * 图片上传
     *
     * @param file file
     * @return R
     */
    @ApiOperation(value = "图片上传")
    @CrossOrigin
    @PostMapping(value = "/file")
    public R upload(@RequestParam(value = "file") MultipartFile file) {
        return R.success(fileService.uploadImg(file));
    }
}
