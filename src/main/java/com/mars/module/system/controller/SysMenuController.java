package com.mars.module.system.controller;

import com.mars.common.enums.BusinessType;
import com.mars.common.result.R;
import com.mars.common.request.sys.SysMenuAddRequest;
import com.mars.common.request.sys.SysMenuQueryRequest;
import com.mars.common.request.sys.SysMenuUpdateRequest;
import com.mars.framework.annotation.Log;
import com.mars.module.system.service.ISysMenuService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import lombok.AllArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 菜单管理控制器
 *
 * @author 源码字节-程序员Mars
 */
@RestController
@RequestMapping("/sys/menu")
@Api(tags = "系统管理-菜单管理")
@AllArgsConstructor
public class SysMenuController {

    private final ISysMenuService sysMenuService;

    /**
     * 获取菜单树
     *
     * @param queryDto 查询
     * @return R
     */
    @PostMapping("/list")
    @ApiOperation(value = "获取菜单树")
    public R list(@Validated @RequestBody SysMenuQueryRequest queryDto) {
        return R.success(sysMenuService.tree(queryDto));
    }

    /**
     * 获取详情
     *
     * @param id id
     * @return R
     */
    @GetMapping("/get/{id}")
    @ApiOperation(value = "获取详情")
    public R get(@PathVariable("id") Long id) {
        return R.success(sysMenuService.get(id));
    }

    /**
     * 新增
     *
     * @param addDto addDto
     * @return R
     */
    @Log(title = "菜单新增", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ApiOperation(value = "新增")
    public R add(@Validated @RequestBody SysMenuAddRequest addDto) {
        sysMenuService.add(addDto);
        return R.success();
    }

    /**
     * 更新
     *
     * @param updateDto update
     * @return R
     */
    @Log(title = "菜单更新", businessType = BusinessType.UPDATE)
    @PostMapping("/update")
    @ApiOperation(value = "修改")
    public R update(@Validated @RequestBody SysMenuUpdateRequest updateDto) {
        sysMenuService.update(updateDto);
        return R.success();
    }

    /**
     * 删除
     *
     * @param id id
     * @return R
     */
    @Log(title = "菜单删除", businessType = BusinessType.DELETE)
    @PostMapping("/delete/{id}")
    @ApiOperation(value = "删除")
    public R delete(@PathVariable("id") Long id) {
        sysMenuService.delete(id);
        return R.success();
    }

    /**
     * 获取一级目录列表
     *
     * @return R
     */
    @GetMapping("/getParentMenu")
    @ApiOperation(value = "获取一级目录列表")
    public R getParentMenu() {
        return R.success(sysMenuService.getParentMenu());
    }
}
