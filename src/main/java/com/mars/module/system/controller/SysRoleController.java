package com.mars.module.system.controller;

import com.mars.common.enums.BusinessType;
import com.mars.common.result.R;
import com.mars.common.request.sys.SysRoleQueryRequest;
import com.mars.common.request.sys.SysRoleAddRequest;
import com.mars.common.request.sys.SysRoleUpdateRequest;
import com.mars.framework.annotation.Log;
import com.mars.module.system.service.impl.SysRoleServiceImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 角色管理控制器
 *
 * @author 源码字节-程序员Mars
 */
@RestController
@RequestMapping("/sys/role")
@Api(tags = "系统管理-角色管理")
@AllArgsConstructor
public class SysRoleController {

    private final SysRoleServiceImpl sysRoleServiceImpl;

    /**
     * 获取列表不分页
     *
     * @return R
     */
    @PostMapping("/all")
    @ApiOperation(value = "获取列表，不分页")
    public R all() {
        return R.success(sysRoleServiceImpl.selectAll());
    }

    /**
     * 获取列表
     *
     * @param sysRoleQueryDto sysRoleQueryDto
     * @return R
     */
    @PostMapping("/list")
    @ApiOperation(value = "获取列表")
    public R list(@Validated @RequestBody SysRoleQueryRequest sysRoleQueryDto) {
        return R.success(sysRoleServiceImpl.list(sysRoleQueryDto));
    }

    /**
     * 获取详情
     *
     * @param id id
     * @return R
     */
    @GetMapping("/get/{id}")
    @ApiOperation(value = "获取详情")
    public R get(@PathVariable("id") Long id) {
        return R.success(sysRoleServiceImpl.get(id));
    }

    /**
     * 新增
     *
     * @param addDto addDto
     * @return R
     */
    @Log(title = "角色新增", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ApiOperation(value = "新增")
    public R add(@Validated @RequestBody SysRoleAddRequest addDto) {
        sysRoleServiceImpl.add(addDto);
        return R.success();
    }

    /**
     * 修改
     *
     * @param request request
     * @return R
     */
    @PostMapping("/update")
    @ApiOperation(value = "修改")
    @Log(title = "角色修改", businessType = BusinessType.UPDATE)
    public R update(@Validated @RequestBody SysRoleUpdateRequest request) {
        sysRoleServiceImpl.update(request);
        return R.success();
    }

    /**
     * 删除
     *
     * @param id id
     * @return R
     */
    @Log(title = "角色删除", businessType = BusinessType.DELETE)
    @PostMapping("/delete/{id}")
    @ApiOperation(value = "删除")
    public R delete(@PathVariable("id") Long id) {
        sysRoleServiceImpl.delete(id);
        return R.success();
    }
}
