package com.mars.module.system.service;


import com.mars.common.request.sys.*;
import com.mars.common.response.PageInfo;
import com.mars.common.response.sys.*;
import com.mars.module.system.entity.SysUser;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
;

/**
 * 用户Service
 *
 * @author 源码字节-程序员Mars
 */
public interface ISysUserService {

    /**
     * 通过名称查询
     *
     * @param userName 用户名
     * @return SysUser
     */
    SysUser selectByUserName(String userName);

    /**
     * 通过id查询
     *
     * @param id id
     * @return SysUserDetailResponse
     */
    SysUserDetailResponse get(Long id);

    /**
     * 分页列表
     *
     * @param queryDto queryDto
     * @return PageVo<SysUserListResponse>
     */
    PageInfo<SysUserListResponse> pageList(SysUserQueryRequest queryDto);

    /**
     * 添加用户
     *
     * @param addDto addDto
     */
    void add(SysUserAddRequest addDto);

    /**
     * 删除
     *
     * @param id id
     */
    void delete(Long id);

    /**
     * 更新
     *
     * @param updateDto updateDto
     */
    void update(SysUserUpdateRequest updateDto);

    /**
     * 更新
     *
     * @param updateDto updateDto
     */
    void dataUpdate(DataUpdateRequest updateDto);

    /**
     * 登录
     *
     * @param loginDto loginDto
     * @return LoginResponse
     */
    LoginResponse login(LoginRequest loginDto, HttpServletRequest request, HttpServletResponse response);

    /**
     * 获取用户信息
     *
     * @param userId 用户ID
     * @return UserInfoResponse
     */
    UserInfoResponse getInfo(Long userId, HttpServletRequest request, HttpServletResponse response);

    /**
     * 退出登录
     *
     * @param request request
     */
    void logout(HttpServletRequest request);

}
