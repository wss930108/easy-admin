package com.mars.module.system.service;

import com.mars.common.request.sys.SysRoleAddRequest;
import com.mars.common.request.sys.SysRoleQueryRequest;
import com.mars.common.request.sys.SysRoleUpdateRequest;
import com.mars.common.response.PageInfo;
import com.mars.common.response.sys.SysRoleDetailResponse;
import com.mars.common.response.sys.SysRoleListResponse;


import java.util.List;

/**
 * 角色Service
 *
 * @author 源码字节-程序员Mars
 */
public interface ISysRoleService {
    /**
     * 查询所有
     *
     * @return List<SysRoleListResponse>
     */
    List<SysRoleListResponse> selectAll();

    /**
     * 分页查询
     *
     * @param queryDto queryDto
     * @return PageVo<SysRoleListResponse>
     */
    PageInfo<SysRoleListResponse> list(SysRoleQueryRequest queryDto);

    /**
     * 详情查询
     *
     * @param id id
     * @return SysRoleDetailResponse
     */
    SysRoleDetailResponse get(Long id);

    /**
     * 添加
     *
     * @param addDto addDto
     */
    void add(SysRoleAddRequest addDto);

    /**
     * 删除
     *
     * @param id id
     */
    void delete(Long id);

    /**
     * 更新
     *
     * @param updateDto updateDto
     */
    void update(SysRoleUpdateRequest updateDto);
}
