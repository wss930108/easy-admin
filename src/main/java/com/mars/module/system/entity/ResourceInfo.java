package com.mars.module.system.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 功能描述
 *
 * @author Mars.wq [wqexpore@163.com]
 * @version 1.0
 * @date 2023-10-20 20:40:56
 */
@Data
@Builder
@Accessors(chain = true)
public class ResourceInfo {

    @TableId(type = IdType.AUTO)
    private Integer id;

    private Integer type;

    private String businessTitle;

    private String businessTime;

    private String mobile;

    private String businessPrice;

    private String address;

    private String contacts;


}
