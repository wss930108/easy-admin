package com.mars.module.admin.controller;


import java.util.Arrays;
import java.util.List;

import com.mars.common.enums.BusinessType;
import com.mars.common.result.R;
import io.swagger.annotations.Api;
import com.mars.framework.annotation.Log;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;
import lombok.extern.slf4j.Slf4j;
import com.mars.module.admin.service.ISysConfigService;
import com.mars.module.admin.request.SysConfigRequest;

/**
 * 系统配置控制层
 *
 * @author mars
 * @date 2023-11-20
 */
@Slf4j
@AllArgsConstructor
@RestController
@Api(value = "系统配置接口管理", tags = "系统配置接口管理")
@RequestMapping("/admin/sysConfig")
public class SysConfigController {

    private final ISysConfigService iSysConfigService;

    /**
     * 分页查询系统配置列表
     */
    @ApiOperation(value = "分页查询系统配置列表")
    @PostMapping("/pageList")
    public R list(@RequestBody SysConfigRequest sysConfig) {
        return R.success(iSysConfigService.pageList(sysConfig));
    }

    /**
     * 获取系统配置详细信息
     */
    @ApiOperation(value = "获取系统配置详细信息")
    @GetMapping(value = "/query/{id}")
    public R getInfo(@PathVariable("id") Long id) {
        return R.success(iSysConfigService.getById(id));
    }

    /**
     * 获取系统配置
     *
     * @param configList configList
     * @return R
     */
    @ApiOperation(value = "获取配置信息")
    @GetMapping(value = "/sysInfo")
    public R sysLoginConfig(@RequestParam(value = "configList",required = false) List<String> configList) {
        return R.success(iSysConfigService.sysLoginConfig(configList));
    }

    /**
     * 新增系统配置
     */
    @Log(title = "新增系统配置", businessType = BusinessType.INSERT)
    @ApiOperation(value = "新增系统配置")
    @PostMapping("/add")
    public R add(@RequestBody SysConfigRequest sysConfig) {
        iSysConfigService.add(sysConfig);
        return R.success();
    }

    /**
     * 修改系统配置
     */
    @Log(title = "修改系统配置", businessType = BusinessType.UPDATE)
    @ApiOperation(value = "修改系统配置")
    @PostMapping("/update")
    public R edit(@RequestBody SysConfigRequest sysConfig) {
        iSysConfigService.update(sysConfig);
        return R.success();
    }

    /**
     * 删除系统配置
     */
    @Log(title = "删除系统配置", businessType = BusinessType.DELETE)
    @ApiOperation(value = "删除系统配置")
    @PostMapping("/delete/{ids}")
    public R remove(@PathVariable Long[] ids) {
        iSysConfigService.deleteBatch(Arrays.asList(ids));
        return R.success();
    }
}
