package com.mars.module.admin.controller;


import java.util.Arrays;

import com.mars.common.enums.BusinessType;
import com.mars.common.result.R;
import io.swagger.annotations.Api;
import com.mars.framework.annotation.Log;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;
import lombok.extern.slf4j.Slf4j;
import com.mars.module.admin.service.ISysDictTypeService;
import com.mars.module.admin.request.SysDictTypeRequest;

/**
 * 字典类型控制层
 *
 * @author mars
 * @date 2023-11-18
 */
@Slf4j
@AllArgsConstructor
@RestController
@Api(value = "字典类型接口管理" , tags = "字典类型接口管理")
@RequestMapping("/admin/sysDictType")
public class SysDictTypeController {

    private final ISysDictTypeService iSysDictTypeService;

    /**
     * 分页查询字典类型列表
     */
    @ApiOperation(value = "分页查询字典类型列表")
    @PostMapping("/pageList")
    public R pageList(@RequestBody SysDictTypeRequest sysDictType) {
        return R.success(iSysDictTypeService.pageList(sysDictType));
    }

    /**
     * 查询所有字典类型列表
     */
    @ApiOperation(value = "查询所有字典类型列表")
    @GetMapping("/list")
    public R list() {
        return R.success(iSysDictTypeService.list());
    }

    /**
     * 获取字典类型详细信息
     */
    @ApiOperation(value = "获取字典类型详细信息")
    @GetMapping(value = "/query/{id}")
    public R getInfo(@PathVariable("id") Long id) {
        return R.success(iSysDictTypeService.getById(id));
    }

    /**
     * 新增字典类型
     */
    @Log(title = "新增字典类型" , businessType = BusinessType.INSERT)
    @ApiOperation(value = "新增字典类型")
    @PostMapping("/add")
    public R add(@RequestBody SysDictTypeRequest sysDictType) {
        iSysDictTypeService.add(sysDictType);
        return R.success();
    }

    /**
     * 修改字典类型
     */
    @Log(title = "修改字典类型" , businessType = BusinessType.UPDATE)
    @ApiOperation(value = "修改字典类型")
    @PostMapping("/update")
    public R edit(@RequestBody SysDictTypeRequest sysDictType) {
        iSysDictTypeService.update(sysDictType);
        return R.success();
    }

    /**
     * 删除字典类型
     */
    @Log(title = "删除字典类型" , businessType = BusinessType.DELETE)
    @ApiOperation(value = "删除字典类型")
    @PostMapping("/delete/{ids}")
    public R remove(@PathVariable Long[] ids) {
        iSysDictTypeService.deleteBatch(Arrays.asList(ids));
        return R.success();
    }
}
