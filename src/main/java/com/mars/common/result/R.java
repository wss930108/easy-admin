package com.mars.common.result;

import com.mars.common.enums.HttpCodeEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 统一响应体
 *
 * @author Mars
 */
@Data
@ApiModel("统一响应结果")
public class R implements Serializable {

    private static final long serialVersionUID = -637415113996231595L;

    @ApiModelProperty("响应时间")
    private LocalDateTime timestamp;

    @ApiModelProperty("响应状态值")
    private String code;

    @ApiModelProperty("响应消息")
    private String message;

    @ApiModelProperty("响应数据")
    private Object data;


    public R(String code, String message, Object data) {
        this.timestamp = LocalDateTime.now();
        this.code = code;
        this.message = message;
        this.data = data;
    }

    public static R success(Object data) {
        return new R(HttpCodeEnum.SUCCESS.getCode(), HttpCodeEnum.SUCCESS.getMessage(), data);
    }

    public static R success() {
        return new R(HttpCodeEnum.SUCCESS.getCode(), HttpCodeEnum.SUCCESS.getMessage(), null);
    }

    public static R error() {
        return new R(HttpCodeEnum.ERROR.getCode(), HttpCodeEnum.ERROR.getMessage(), null);
    }

    public static R error(String message) {
        return new R(HttpCodeEnum.ERROR.getCode(), message, null);
    }

    public static R error(String code, String message) {
        return new R(code, message, null);
    }

    public R(String status, String message) {
        this(status, message, null);
    }

    public R() {
    }
}
