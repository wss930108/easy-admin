package com.mars.module.admin.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.util.*;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import cn.afterturn.easypoi.excel.annotation.Excel;
import java.time.LocalDateTime;
import com.mars.module.system.entity.BaseEntity;

    /**
 * 测试对象 ap_test
 *
 * @author mars
 * @date 2023-11-21
 */

@Data
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@ApiModel(value = "测试对象")
@Accessors(chain = true)
@TableName("ap_test")
public class ApTest extends BaseEntity {


    /**
     * ID
     */
    @TableId(value = "id", type = IdType.AUTO)
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    @ApiModelProperty(value = "id")
    private Long id;

    /**
     * 名称
     */
    @Excel(name = "名称")
    @ApiModelProperty(value = "名称")
    private String name;

    /**
     * 年龄
     */
    @Excel(name = "年龄")
    @ApiModelProperty(value = "年龄")
    private String age;

    /**
     * 图片
     */
    @Excel(name = "图片")
    @ApiModelProperty(value = "图片")
    private String picture;

    /**
     * 创建时间
     */
    @Excel(name = "创建时间")
    @ApiModelProperty(value = "创建时间")
    private LocalDateTime createTime;
}
