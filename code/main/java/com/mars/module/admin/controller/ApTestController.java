package com.mars.module.admin.controller;


import java.util.Arrays;
import com.mars.common.enums.BusinessType;
import com.mars.common.result.R;
import io.swagger.annotations.Api;
import com.mars.framework.annotation.Log;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;
import lombok.extern.slf4j.Slf4j;
import com.mars.module.admin.service.IApTestService;
import com.mars.module.admin.request.ApTestRequest;

/**
 * 测试控制层
 *
 * @author mars
 * @date 2023-11-21
 */
@Slf4j
@AllArgsConstructor
@RestController
@Api(value = "测试接口管理",tags = "测试接口管理")
@RequestMapping("/admin/apTest" )
public class ApTestController {

    private final IApTestService iApTestService;

    /**
     * 分页查询测试列表
     */
    @ApiOperation(value = "分页查询测试列表")
    @PostMapping("/pageList")
    public R list(@RequestBody ApTestRequest apTest) {
        return R.success(iApTestService.pageList(apTest));
    }

    /**
     * 获取测试详细信息
     */
    @ApiOperation(value = "获取测试详细信息")
    @GetMapping(value = "/query/{id}" )
    public R getInfo(@PathVariable("id" ) Long id) {
        return R.success(iApTestService.getById(id));
    }

    /**
     * 新增测试
     */
    @Log(title = "新增测试", businessType = BusinessType.INSERT)
    @ApiOperation(value = "新增测试")
    @PostMapping("/add")
    public R add(@RequestBody ApTestRequest apTest) {
        iApTestService.add(apTest);
        return R.success();
    }

    /**
     * 修改测试
     */
    @Log(title = "修改测试", businessType = BusinessType.UPDATE)
    @ApiOperation(value = "修改测试")
    @PostMapping("/update")
    public R edit(@RequestBody ApTestRequest apTest) {
        iApTestService.update(apTest);
        return R.success();
    }

    /**
     * 删除测试
     */
    @Log(title = "删除测试", businessType = BusinessType.DELETE)
    @ApiOperation(value = "删除测试")
    @PostMapping("/delete/{ids}" )
    public R remove(@PathVariable Long[] ids) {
        iApTestService.deleteBatch(Arrays.asList(ids));
        return R.success();
    }
}
